# Especificação do manifesto plasc

A especificação do manifesto plasc estabelece um padrão de definição do arquivo de manifesto do plasc. Este arquivo, por sua vez, é o principal insumo utilizado pelo plasc framework. As especificações podem ser vistas [aqui](docs/00-overview.md).

## Exemplo ilustrativo

Aqui está um exemplo de um manifesto escrito em `YAML` que utiliza os recursos `model`, `store` e `components`:

```yml
spec: my-spec.json
resources:
  - name: foo
    type: model
    attributes:
      - name: body
        type: text
        validations:
          absence: true

  - type: store
    name: session
    data:
      - session
    source: getSession

  - type: component
    name: Doctors
    module: doctors
    children:
      - HeaderHome
      - TitleAndDescription
      - DoctorsSearch
```

## Changelog

O changelog da especificação do manifesto plasc pode ser visto [aqui](docs/CHANGELOG.md).

## License

MIT
