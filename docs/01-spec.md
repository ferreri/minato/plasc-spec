# Spec

`spec` é a definição de como o manifesto será validado. As validações do manifesto são feitas com base em um JSON Schema.

```yml
spec: https://my-spec.json
```

- `spec`: propriedade obrigatória que específica o caminho para o seu arquivo JSON Schema. Caso não seja definido será utilizada a especificação padrão do plasc.
