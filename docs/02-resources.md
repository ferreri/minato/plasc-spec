# Resources

`resources` é a definção base do manifesto plasc. Aqui é definido quais são os recursos presentes na sua aplicação que serão utilizados pelo plasc.

## Exemplo ilustrativo

```yml
resources:
  - name: foo
    type: model
    attributes:
      - name: body
        type: text
        validations:
          absence: true

  - type: store
    name: session
    data:
      - session
    source: getSession

  - type: component
    name: Doctors
    module: doctors
    children:
      - HeaderHome
      - TitleAndDescription
      - DoctorsSearch
```

|Estes são os recursos suportados pelo plasc.

### model

`model` define um modelo utilizado em um serviço de API. Para a definição completa de `model` veja [Model](03-model.md).

### store

`store` define a camada de dados de um serviço web. Para a definição completa de `store` veja [Store](04-store.md).

### component

`component` define a interface (UI) de um serviço web. Para a definição completa de `component` veja [Component](05-component.md).
