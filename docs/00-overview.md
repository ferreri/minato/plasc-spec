# Especificação do arquivo plasc

O arquivo de especificação foi dividido nas seguintes páginas:

- [Spec](01-spec.md)
- [Resources](02-resources.md)
- [Model](03-model.md)
  - [Model Validations](06-model-validations.md)
  - [Model Associations](07-model-associations.md)
- [Store](04-store.md)
- [Component](05-component.md)
