import Ajv from 'ajv';
import schema from '../schema/plasc-spec.json';

const validator = new Ajv();
const testSuiteFiles = [
  require('./tests/plasc-spec.tests.json'),
  require('./tests/component-resources.tests.json'),
  require('./tests/store-resources.tests.json'),
  require('./tests/model-resources.tests.json'),
  require('./tests/model_attributes-resources.tests.json'),
  require('./tests/model_validations-resources.tests.json'),
  require('./tests/model_associations-resources.tests.json')
];

describe('JSON Schema Test Suits', () => {
  testSuiteFiles.forEach((testDefinitions) => {
    testDefinitions.forEach(({ context, tests }) => {
      describe(context, () => {
        tests.forEach((test) => {
          it(test.description, () => {
            expect(validator.validate(schema, test.data)).toBe(test.valid);
          });
        });
      });
    });
  });
});
