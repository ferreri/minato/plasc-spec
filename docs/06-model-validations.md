# Model Validations

A propriedade `validations` define quais validações devem ser aplicadas no atributo durante as ações de `create` ou `update` de um modelo.
As validações definidas no manifesto devem refletir às validações definidas no modelo, como por exemplo:

```yml
# plasc-manifest.yml

resources:
  - name: user
    type: model
    attributes:
      - name: email
        type: string
        validations:
          presence: true
```

```ruby
# app/models/user.rb

class User < ActiveRecord
  validates :email, presence: true
end

```

## Estas são as validaçãoes suportadas pelo plasc:

> **Atenção:** Toda validação pode ter um conjunto de propriedades comuns - que não são obrigatórias - mas que estão disponíveis para uso em qualquer validação. Essas propriedades são:<br/>
>
> - `allow_blank: boolean` - permite verificar se a validação aceita valores em branco para o atributo.
> - `allow_nil: boolean` - permite verificar se a validação aceita valores nulo para o atributo.
> - `message: string` - permite validar o uso de uma mensagem personalizada de erro, caso a validação venha a falhar.
> - `on: string` - permite validar em qual ação a validação deve ser executada. Os valores possíveis para este campo são: _"create"_ ou _"update"_.
> - `strict: boolean` - permite validar se a validação deve ser executada no modo _strict_.

### absence

`absence: boolean|object` - Define que o atributo deve ser validado utilizando a validação [absence do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#absence).

```yml
validations:
  absence: true
```

```yml
validations:
  absence:
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

### acceptance

`acceptance: boolean|object` - Define que o atributo deve ser validado utilizando a validação [acceptance do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#acceptance).

```yml
validations:
  acceptance: true
```

```yml
validations:
  acceptance:
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

### confirmation

`confirmation: boolean|object` - Define que o atributo deve ser validado utilizando a validação [confirmation do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#confirmation).

```yaml
validations:
  confirmation: true
```

```yml
validations:
  confirmation:
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

<!--### comparison

 `comparison: object` - Define que o atributo deve ser validado utilizando a validação [comparison do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#comparison).
  ```yaml
  validations:
    comparison:
      greater_than: 'foo bar'
      greater_than_or_equal_to: 'foo bar'
      equal_to: 50
      less_than: 99
      less_than_or_equal_to: 99
      other_than: 'bar foo'
      allow_blank: true
      allow_nil: true
      message: 'my error custom message'
      on: create
      strict: true
  ``` -->

### exclusion

`exclusion: object` - Define que o atributo deve ser validado utilizando a validação [exclusion do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#exclusion).

```yaml
validations:
  exclusion:
    in:
      - a
      - b
      - c
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

- `in: string[] (obrigatório)` - define a lista de valores não permitidos para o atributo.

### format

`format: object` - Define que o atributo deve ser validado utilizando a validação [format do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#format).

```yaml
validations:
  format:
    with: 'foo'
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

- `with: Regexp pattern (obrigatório)` - define o formato esperado para o atributo. O valor deve ser um _pattern_ válido de um [Regexp](https://ruby-doc.org/core-3.1.0/Regexp.html), sem o delimitador ( _/_ ). Ex.: Para o Regexp `/(\w+)/` você deve informar apenas `(\w+)`.

### inclusion

`inclusion: object` - Define que o atributo deve ser validado utilizando a validação [inclusion do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#inclusion).

```yaml
validations:
  inclusion:
    in:
      - 99
      - 88
      - 77
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

- `in: string[]|number[] (obrigatório)` - define a lista de valores permitidos para o atributo.

### length

`length: object` - Define que o atributo deve ser validado utilizando a validação [length do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#length).

```yaml
validations:
  length:
    minimum: 1
    maximum: 10
    is: 99
    wrong_length: 'foo bar'
    too_long: 'foo bar'
    too_short: 'foo bar'
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

- `minimum: integer` - define o valor do length mínimo permitido.
- `maximum: integer` - define o valor do lenght máximo permitido.
- `is: integer` - define o valor do length exato.
- `wrong_length: string` - permite validar o uso de uma mensagem personalizada de erro, caso a validação venha a falhar por _wrong_length_.
- `too_long: string` - permite validar o uso de uma mensagem personalizada de erro, caso a validação venha a falhar por _too_long_.
- `too_short: string` - permite validar o uso de uma mensagem personalizada de erro, caso a validação venha a falhar por _too_short_.
  > Deve-se informar pelo menos uma das propriedades: minimum, maximum ou is. Caso o length seja um range, as propriedades minimum e maximum devem ser informadas em conjunto.

### numericality

`numericality: boolean|object` - Define que o atributo deve ser validado utilizando a validação [numericality do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#numericality).

```yaml
validations:
  numericality: true
```

```yml
validations:
  numericality:
    only_integer: true
    greater_than: 1
    greater_than_or_equal_to: 1
    equal_to: 50
    less_than: 99
    less_than_or_equal_to: 99
    other_than: 5
    is_in:
      - 1
      - 2
      - 3
    odd: true
    even: false
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

- `only_integer: boolean` - define a propriedade only_integer da validação.
- `greater_than: number` - define o valor da propriedade greater_than da validação.
- `greater_than_or_equal_to: number` -define o valor da propriedade greater_than_or_equal_to da validação.
- `equal_to: number` - define o valor da propriedade equal_to da validação.
- `less_than: number` - define o valor da propriedade less_than da validação.
- `less_than_or_equal_to: number` - define o valor da propriedade less_than_or_equal_to da validação.
- `other_than: number` - define o valor da propriedade other_than da validação.
- `is_in: number[]` - define o valor da propriedade is_in da validação.
- `odd: boolean` - define o valor da propriedade odd da validação.
- `even: boolean` - define o valor da propriedade even da validação.
  > Deve-se definir pelo menos uma dessas propriedades ao utilizar essa validação.

### presence

`presence: boolean|object` - Define que o atributo deve ser validado utilizando a validação [presence do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#presence).

```yaml
validations:
  presence: true
```

```yml
validations:
  presence:
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

### uniqueness

`uniqueness: boolean|object` - Define que o atributo deve ser validado utilizando a validação [uniqueness do Rails Active Record](https://guides.rubyonrails.org/active_record_validations.html#uniqueness).

```yaml
validations:
  uniqueness: true
```

```yml
validations:
  uniqueness:
    scope: 'foo bar'
    allow_blank: true
    allow_nil: true
    message: 'my error custom message'
    on: create
    strict: true
```

- `scope: string` - define a propriedade scope da validação.
