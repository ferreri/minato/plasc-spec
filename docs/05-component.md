# Component

`component` representa a interface (UI) de um serviço web.

```yml
- name: DoctorSearch
  type: component
  module: doctors
  children:
    - InputSearch
  props:
    - onChange
    - serialize
    - deserialize
```

### name (obrigatório)

`name: string` - define o nome de um componente.

### type (obrigatório)

`type: string` - define o tipo de um recurso. Para componente deve-se utilizar o valor `component`.

### module (obrigatório)

`module: string` - define o módulo onde pode ser encontrado o componente.

### children (obrigatório)

`children: string[]` - define os filhos (composição) de um componente.

### props (opcional)

`props: string[]` - define as propriedades de um componente.
