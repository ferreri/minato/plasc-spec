FROM node:16-alpine

ARG GITLAB_AUTH_TOKEN
ARG BASE_PATH

RUN mkdir -p /app
WORKDIR /app

COPY package.json yarn.lock ./
ENV GITLAB_AUTH_TOKEN ${GITLAB_AUTH_TOKEN}
RUN yarn install --pure-lockfile --non-interactive
COPY . ./

CMD ["yarn"]
