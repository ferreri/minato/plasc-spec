# Model

`model` representa o modelo de dados de um serviço de API.

```yml
resources:
  - name: foo
    type: model
    attributes:
      - name: body
        type: text
        validations:
          absence: true
```

### name (obrigatório)

`name: string` - define o nome de um modelo.

### type (obrigatório)

`type: string` - define o tipo de um recurso. Para modelos deve-se utilizar o valor `model`.

### attributes (obrigatório)

`attributes: object` - define os atributos de um modelo de acordo com as definições a seguir:

- `name: string (obrigatório)` - define o nome do atributo.
- `type: string (obrigatório)` - define o tipo do atributo.
- `validations: object` - define quais validações devem ser aplicadas no atributo. Para mais informações veja [Model Validations](06-model-validations.md).
