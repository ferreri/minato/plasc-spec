# Model Associations

A propriedade `associations` define a conexão entre os modelos.

As associações definidas no manifesto devem refletir às associações definidas no modelo, como por exemplo:

```yml
# plasc-manifest.yml

resources:
  - name: section
    type: model
    associations:
      - name: belongs_to
        reference: document
      - name: has_many
        reference: paragraphs
```

```ruby
# app/models/section.rb

class Section < ApplicationRecord
  belongs_to :document
  has_many :paragraphs
end
```

## Estas são as associações suportadas pelo plasc:

> **Atenção:** Toda associação pode ter um conjunto de propriedades comuns - que não são obrigatórias - mas que estão disponíveis para uso em qualquer associação. Essas propriedades são:<br/>

> - `name: string` - permite verificar qual a associação será validada.
> - `reference: string` - permite verificar o modelo associado.
> - `options: object` - permite verificar as opções definidas no modelo:
>   > - `as: boolean` - permite verificar se a opção `as` foi especificada no modelo.
>   > - `autosave: boolean` - permite verificar se a opção `autosave` foi especificada no modelo.
>   > - `class_name: string` - permite verificar se a opção `class_name` foi especificada no modelo.
>   > - `conditions: string` - permite verificar se a opção `conditions` foi especificada no modelo.
>   > - `counter_cache: boolean` - permite verificar se a opção `counter_cache` foi especificada no modelo.
>   > - `dependent: boolean/string` - permite verificar se a opção `dependent` foi especificada no modelo.
>   > - `with_primary_key: string` - permite verificar se a opção `with_primary_key` foi especificada no modelo.
>   > - `with_foreign_key: string` - permite verificar se a opção `with_foreign_key` foi especificada no modelo.
>   > - `without_validating_presence: boolean` - permite verificar se a opção `without_validating_presence` foi especificada no modelo.
>   > - `index_errors: boolean` - permite verificar se a opção `index_errors` foi especificada no modelo.
>   > - `inverse_of: string` - permite verificar se a opção `inverse_of` foi especificada no modelo.
>   > - `join_table: string` - permite verificar se a opção `join_table` foi especificada no modelo.
>   > - `required: boolean` - permite verificar se a opção `required` foi especificada no modelo.
>   > - `source: string` - permite verificar se a opção `source` foi especificada no modelo.
>   > - `through: string` - permite verificar se a opção `through` foi especificada no modelo.
>   > - `touch: boolean` - permite verificar se a opção `touch` foi especificada no modelo.
>   > - `validate: boolean` - permite verificar se a opção `validate` foi especificada no modelo.
>   > - `optional: boolean` - permite verificar se a opção `optional` foi especificada no modelo.
>   > - `order: string` - permite verificar se a opção `order` foi especificada no modelo.

### belongs_to

`belongs_to: object` - Define que a associação [belongs_to](https://guides.rubyonrails.org/association_basics.html#belongs-to-association-reference) existe no modelo

```yml
- name: belongs_to
  reference: association_model
  options:
    autosave: true
    with_foreign_key: foreign_id
```

### has_one

`has_one: object` - Define que a associação [has_one](https://guides.rubyonrails.org/association_basics.html#has-one-association-reference) existe no modelo

```yml
- name: has_one
  reference: association_model
```

### has_many

`has_many: object` - Define que a associação [has_many](https://guides.rubyonrails.org/association_basics.html#has-many-association-reference)
existe no modelo

```yml
- name: has_many
  reference: association_model
  options:
    class_name: Patron
```

### has_and_belongs_to_many

`has_and_belongs_to_many: object` - Define que a associação [has_and_belongs_to_many](https://guides.rubyonrails.org/association_basics.html#has-and-belongs-to-many-association-reference) existe no modelo

```yml
- name: has_and_belongs_to_many
  reference: association_model
  options:
    join_table: :assembles
```

- `:join_table` - permite verificar se a opção `join_table` foi especificada no modelo.
