# Changelog

Todas as mudanças importantes do para este projeto serão documentadas neste arquivo.

O formato segue as especificações do [Keep a changelog](https://keepachangelog.com/) e este projeto utiliza o [Semantic Versioning](https://semver.org/).

## [v1.0.0](https://gitlab.com/ferreri/minato/plasc-spec/-/releases/v1.0.0) - 2023-06-16

### Adicionado
- Recursos no schema: component, store, model e model-validations
- Nova documentação do schema
### Alterado
- Foi feito refatorações nas propriedades de model e model-assertions
- Recursos de store e component agora tem suporte para propriedade `module`
### Corrigido
- Foi feita a remoção de uma ref que não estava mais sendo utilizada e que causava inconsistências no schema
