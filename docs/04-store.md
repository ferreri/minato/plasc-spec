# Store

`store` representa a camada de dados de um serviço web. Define onde os dados são armazenados e gerenciados.

```yml
- name: useDoctors
  type: store
  module: doctors
  data:
    - doctors
    - isLoading
  source: getDoctors
  method:
    - refetch
```

### name (obrigatório)

`name: string` - define o nome de uma store.

### type (obrigatório)

`type: string` - define o tipo de um recurso. Para store deve-se utilizar o valor `store`.

### module (obrigatório)

`module: string` - define o módulo onde pode ser encontrado a store.

### data (obrigatório)

`data: string[]` - define os dados que serão retornados pela store.

### source (obrigatório)

`source: string` - define a função de onde serão buscados os dados da store.

### method (opcional)

`method: string[]` - define quais metódos serão retornados pela store.

### file (opcional)

`file: string` - define em qual arquivo a store se encontra.
